package com.example.renat.example_asynctaskloader_sqlite;


import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{
    private EditText editText1, editText2, editText3;
    private Button buttonGetNext, buttonSave;
    private Spinner spinner;
    private int spinnerSelectIndex = 0;
    private ArrayAdapter<String> spinnerAdapter;
    private int entryId;
    private int startIndex = 1;

    private String KEY_SPINN_SELECT_INDEX = "key_spinn_select_index";
    private String KEY_ENTRY_ID = "key_entry_id";
    private String KEY_START_INDEX = "start_index";

    static final int LOADER_TABLE_LIST = 1;
    static final int LOADER_GET_ENTRIES = 2;
    static final int LOADER_UPDATE_ENTRY = 3;
    public static DataBaseHelper dataBaseHelper;

    public MainFragment()
    {
        // Required empty public constructor
    }

    // TODO: AsyncTaskLoader - MainFragment реализует интерфейс LoaderManager.LoaderCallbacks
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle)
    {
        Loader<Cursor> loader = null;
        switch (id)
        {
            case LOADER_TABLE_LIST:
                loader = new TableListLoader(getActivity(), bundle);
                break;
            case LOADER_GET_ENTRIES:
                loader = new GetEntriesFromDB(getActivity(), bundle);
                break;
            case LOADER_UPDATE_ENTRY:
                loader = new UpdateEntryDB(getActivity(), bundle);
                break;
        }
        return loader;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        try
        {
            spinnerSelectIndex = spinner.getSelectedItemPosition();
            outState.putInt(KEY_SPINN_SELECT_INDEX, spinnerSelectIndex);
            outState.putInt(KEY_ENTRY_ID, entryId);
            outState.putInt(KEY_START_INDEX, startIndex);

        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        return;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (dataBaseHelper == null)
        {
            dataBaseHelper = new DataBaseHelper(getActivity());
            dataBaseHelper.createDB(); // TODO: SQLite вызов метода создания БД
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View fragment_view = inflater.inflate(R.layout.fragment_main, container, false);
        editText1 = (EditText) fragment_view.findViewById(R.id.edit_text1);
        editText2 = (EditText) fragment_view.findViewById(R.id.edit_text2);
        editText3 = (EditText) fragment_view.findViewById(R.id.edit_text3);

        buttonGetNext = (Button) fragment_view.findViewById(R.id.button);
        buttonSave = (Button) fragment_view.findViewById(R.id.btn_save);
        spinner = (Spinner) fragment_view.findViewById(R.id.spinner);

        spinner_OnItemSelectedListener();
        buttons_OnClick();

        if (savedInstanceState == null)
        {
            setItemsToSpinner();

        }

        if (savedInstanceState != null)
        {
            try
            {
                // TODO: AsyncTaskLoader инициализация 
                getLoaderManager().initLoader(LOADER_TABLE_LIST, savedInstanceState, MainFragment.this);
                spinnerSelectIndex = savedInstanceState.getInt(KEY_SPINN_SELECT_INDEX);
                entryId = savedInstanceState.getInt(KEY_ENTRY_ID);
                startIndex = savedInstanceState.getInt(KEY_START_INDEX);

            } catch (NullPointerException e)
            {
                e.printStackTrace();
            }
        }

        return fragment_view;
    }

    private void setItemsToSpinner()
    {
        Loader<Cursor> asyncTaskLoader;
        // TODO: AsyncTaskLoader передача параметров в AsyncTaskLoader 
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.key_cmd), getString(R.string.cmd_set_items_spinner));

        // TODO: AsyncTaskLoader запуск загрузки данных
        asyncTaskLoader = getLoaderManager().restartLoader(LOADER_TABLE_LIST, bundle, MainFragment.this);
        asyncTaskLoader.forceLoad();
    }

    @Override // TODO: AsyncTaskLoader реализация интерфейса LoaderManager.LoaderCallbacks
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor)
    {
        //region     Результат работы TableListLoader
        if (loader.getId() == LOADER_TABLE_LIST)
        {
            loadTableListHandler(cursor);
        }
        //endregion
        //region     Результат работы GetEntriesFromDB
        if (loader.getId() == LOADER_GET_ENTRIES)
        {
            loadDbEntryHandler(cursor);
        }
        //endregion
        //region    Результат работы UpdateEntryDB
        if (loader.getId() == LOADER_UPDATE_ENTRY)
        {
            updateDbHandler(cursor);
        }
        //endregion

        if (MainFragment.dataBaseHelper.database.isOpen())
        {
            MainFragment.dataBaseHelper.close();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {

    }

    private void loadTableListHandler(Cursor cursor)
    {
        final ArrayList<String> arrayList = new ArrayList<>();
        String nameNotDict;
        try
        {
            if (cursor != null && cursor.getCount() > 0)
            {
                if (cursor.moveToFirst())
                {
                    while ( !cursor.isAfterLast() )
                    {
                        nameNotDict = cursor.getString( cursor.getColumnIndex("name"));
                        if (!nameNotDict.equals("android_metadata") && !nameNotDict.equals("sqlite_sequence"))
                        {
                            arrayList.add( cursor.getString( cursor.getColumnIndex("name")) );
                        }
                        cursor.moveToNext();
                    }
                }
                spinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, arrayList);
                spinner.setAdapter(spinnerAdapter);
                spinner.setSelection(spinnerSelectIndex);
            }
        } catch (Exception e)
        {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void loadDbEntryHandler(Cursor cursor)
    {
        try
        {
            if (cursor != null && cursor.getCount() == 1)
            {
                if (cursor.moveToFirst())
                {
                    while ( !cursor.isAfterLast() )
                    {
                        editText1.setText(cursor.getString(0));
                        editText2.setText(cursor.getString(1));
                        editText3.setText(cursor.getString(3));
                        entryId = Integer.parseInt(cursor.getString(4));
                        cursor.moveToNext();
                    }
                }
                startIndex++;
            }
            else if (cursor != null && cursor.getCount() <= 0)
            {
                Toast.makeText(getActivity(), R.string.toast_text_end, Toast.LENGTH_SHORT).show();
                startIndex = 1;
            }
        } catch (Exception e)
        {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void updateDbHandler(Cursor cursor)
    {
        try
        {
            if (cursor != null && cursor.getCount() > 0)
            {
                Toast.makeText(getActivity(), R.string.update_db_successful, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e)
        {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void spinner_OnItemSelectedListener()
    {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if (adapterView.getSelectedItemPosition() != spinnerSelectIndex)
                {
                    startIndex = 1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {

            }
        });
    }

    private void buttons_OnClick()
    {
        buttonGetNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.table_name), spinner.getSelectedItem().toString());
                bundle.putInt(getString(R.string.start_index), startIndex);
                bundle.putInt(getString(R.string.end_index), startIndex);
                Loader<Cursor> cursorLoader = getLoaderManager().restartLoader(LOADER_GET_ENTRIES, bundle, MainFragment.this);
                cursorLoader.forceLoad();

            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.table_name), spinner.getSelectedItem().toString());
                bundle.putString(getString(R.string.column_english), editText1.getText().toString());
                bundle.putString(getString(R.string.column_russian), editText2.getText().toString());
                bundle.putInt(getString(R.string.column_count), Integer.parseInt(editText3.getText().toString()));
                bundle.putInt(getString(R.string.column_id), entryId);
                Loader<Cursor> cursorLoader = getLoaderManager().restartLoader(LOADER_UPDATE_ENTRY, bundle, MainFragment.this);
                cursorLoader.forceLoad();
            }
        });
    }




}
