package com.example.renat.example_asynctaskloader_sqlite;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity
{
    MainFragment fragment;
    FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null)
        {
            fragment = new MainFragment();
            transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_frame, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (MainFragment.dataBaseHelper.database.isOpen())
        {
            MainFragment.dataBaseHelper.close();
        }
        finish();
    }
}
