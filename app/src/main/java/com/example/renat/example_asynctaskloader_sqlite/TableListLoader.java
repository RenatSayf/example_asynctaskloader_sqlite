package com.example.renat.example_asynctaskloader_sqlite;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Renat on 14.02.2017.
 */
// TODO: AsyncTaskLoader класс загрузчика TableListLoader
public class TableListLoader extends AsyncTaskLoader<Cursor>
{
    private String command;

    public TableListLoader(Context context, Bundle arg)
    {
        super(context);
        if (arg != null)
        {
            command = arg.getString(getContext().getResources().getString(R.string.key_cmd));
        }
    }

    @Override
    public Cursor loadInBackground()
    {
        if (command != null && command.equals(getContext().getResources().getString(R.string.cmd_set_items_spinner)))
        {
            try
            {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            return GetLictTable();
        }

        return null;
    }

    private Cursor GetLictTable()
    {
        Cursor cursor = null;
        try
        {
            MainFragment.dataBaseHelper.open();
            if (MainFragment.dataBaseHelper.database.isOpen())
            {
                cursor = MainFragment.dataBaseHelper.database.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
            }
        } catch (SQLException e)
        {
            Log.v(getContext().getString(R.string.app_log), e.getMessage());
            e.printStackTrace();
        }
        return cursor;
    }



}
