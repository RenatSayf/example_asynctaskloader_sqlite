package com.example.renat.example_asynctaskloader_sqlite;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Renat on 20.02.2017.
 */

public class UpdateEntryDB extends AsyncTaskLoader<Cursor>
{
    private String tableName, engText, rusText;
    private int id, cont_repeat;

    public UpdateEntryDB(Context context, Bundle args)
    {
        super(context);
        if (args != null)
        {
            this.tableName = args.getString(getContext().getString(R.string.table_name));
            this.engText = args.getString(getContext().getString(R.string.column_english));
            this.rusText = args.getString(getContext().getString(R.string.column_russian));
            this.id = args.getInt(getContext().getString(R.string.column_id));
            this.cont_repeat = args.getInt(getContext().getString(R.string.column_count));
        }
        return;
    }

    @Override
    public Cursor loadInBackground()
    {
        return updateEntryIntoDB(id, tableName, engText, rusText, cont_repeat);
    }

    private Cursor updateEntryIntoDB(int id, String tableName, String engText, String rusText, int contRepeat)
    {
        ContentValues values = new ContentValues();
        values.put(getContext().getString(R.string.column_english), engText);
        values.put(getContext().getString(R.string.column_russian), rusText);
        values.put(getContext().getString(R.string.column_image), "");
        values.put(getContext().getString(R.string.column_count), contRepeat);
        String strId = String.valueOf(id);
        Cursor cursor = null;
        try
        {
            MainFragment.dataBaseHelper.open();
            if (MainFragment.dataBaseHelper.database.isOpen())
            {
                int update = MainFragment.dataBaseHelper.database.update(tableName, values, "_id = " + strId, null);
                if (update > 0)
                {
                    cursor = MainFragment.dataBaseHelper.database.query(true, tableName, new String[]{"English"}, "_id = " + strId, null, null, null, null, null);
                }
            }
        } catch (SQLException e)
        {
            Log.v(getContext().getString(R.string.app_log), e.getMessage());
            e.printStackTrace();
        }
        return cursor;
    }
}
