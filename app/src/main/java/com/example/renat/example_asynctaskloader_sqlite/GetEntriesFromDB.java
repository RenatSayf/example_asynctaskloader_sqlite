package com.example.renat.example_asynctaskloader_sqlite;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Renat on 15.02.2017.
 */
// TODO: AsyncTaskLoader класс загрузчика GetEntriesFromDB
public class GetEntriesFromDB extends AsyncTaskLoader<Cursor>
{
    private String tableName, order;
    private int startIndex, endIndex;

    public GetEntriesFromDB(Context context, Bundle args)
    {
        super(context);
        // TODO: AsyncTaskLoader получение параметров
        if (args != null)
        {
            this.tableName = args.getString(context.getString(R.string.table_name));
            this.order = args.getString(context.getString(R.string.order));
            this.startIndex = args.getInt(context.getString(R.string.start_index));
            this.endIndex = args.getInt(context.getString(R.string.end_index));
        }
    }

    @Override
    public Cursor loadInBackground()
    {
        return getEntriesFromDB(tableName, startIndex, endIndex);
    }

    private Cursor getEntriesFromDB(String tableName, int startId, int endId)
    {
        Cursor cursor = null;
        try
        {
            MainFragment.dataBaseHelper.open();
            if (MainFragment.dataBaseHelper.database.isOpen())
            {
                cursor = MainFragment.dataBaseHelper.database.rawQuery("SELECT * FROM " + tableName + " WHERE RowID BETWEEN " + startId +" AND " + endId, null);
            }
        } catch (SQLException e)
        {
            Log.v(getContext().getString(R.string.app_log), e.getMessage());
            e.printStackTrace();
        }
        return cursor;
    }
}
