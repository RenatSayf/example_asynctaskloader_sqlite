package com.example.renat.example_asynctaskloader_sqlite;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Renat on 12.02.2017.
 */

// TODO: SQLite DataBaseHelper - класс для создания БД из существующей, открытия и закрытия БД
public class DataBaseHelper extends SQLiteOpenHelper
{
    private String DB_PATH;
    private static final String DB_NAME = "sqliteDB"; // название бд
    private static final int version = 1; // версия базы данных

    public SQLiteDatabase database;
    private Context context;

    public DataBaseHelper(Context context)
    {
        super(context, DB_NAME, null, version);
        this.context = context;
        String packageName = context.getApplicationInfo().packageName;
        DB_PATH = "/data/" + packageName + "/databases/";
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1)
    {

    }

    private String getDbPath()
    {
        String dbPath = null;
        File dbFile;
        try
        {
            String mediaMounted = Environment.MEDIA_MOUNTED;
            String storageState = Environment.getExternalStorageState();
            if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
            {
                dbFile = context.getExternalCacheDir();
                dbPath = dbFile.getPath();
            }
            else
            {
                dbPath = Environment.getDataDirectory().toString() + DB_PATH;
            }
        } catch (Exception e)
        {
            Log.v(context.getString(R.string.app_log), e.getMessage());
            e.printStackTrace();
        }

        return dbPath;
    }

    // TODO: SOLite метод создания БД из существующей
    public void createDB()
    {
        File directoryDB = new File(getDbPath());
        String pathToDB = directoryDB.getAbsolutePath() + "/" + DB_NAME;
        File fileDB = new  File(pathToDB);
        boolean exists = fileDB.exists();
        if (!exists)
        {
            try
            {
                if (directoryDB.mkdirs()) // Создание директории для базы данных
                {
                    // если директория создана, создаем базу данных в ней
                    this.database = SQLiteDatabase.openOrCreateDatabase(fileDB, null);
                }
                // Получаем из assets локальную бд как поток
                InputStream inputStream1 = this.context.getAssets().open(DB_NAME + ".db");
                // Открываем пустую бд на устройстве
                FileOutputStream outputStream1 = new FileOutputStream(pathToDB);
                // Запись данных
                byte[] buffer = new byte[1024];
                int length;
                while ((length = inputStream1.read(buffer)) > 0)
                {
                    outputStream1.write(buffer, 0, length);
                }
                outputStream1.flush();
                outputStream1.close();
                inputStream1.close();
            }
            catch (IOException e)
            {
                Log.v(context.getString(R.string.app_log), e.getMessage());
            }
        }
    }

    public void open()
    {
        String dbPath = getDbPath() + DB_NAME;
        try
        {
            database = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
        } catch (Exception e)
        {
            Log.v(context.getString(R.string.app_log), e.getMessage());
        }
    }

    @Override
    public synchronized void close()
    {
        super.close();
        try
        {
            if (database != null)
            {
                database.close();
            }
        } catch (Exception e)
        {
            Log.v(context.getString(R.string.app_log), e.getMessage());
        }
    }
}
