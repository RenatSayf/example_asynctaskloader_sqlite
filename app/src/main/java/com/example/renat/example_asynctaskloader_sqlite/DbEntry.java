package com.example.renat.example_asynctaskloader_sqlite;

/**
 * Created by Renat on 15.02.2017.
 */

public class DbEntry
{
    private String english;
    private String translate;
    private String count_repeat;

    public DbEntry(String english, String translate, String count_repeat)
    {
        this.english =english;
        this.translate =translate;
        this.count_repeat =count_repeat;
    }

    public String getEnglish()
    {
        return english;
    }

    public void setEnglish(String english)
    {
        this.english = english;
    }

    public String getTranslate()
    {
        return translate;
    }

    public void setTranslate(String translate)
    {
        this.translate = translate;
    }

    public String getCount_repeat()
    {
        return count_repeat;
    }

    public void setCount_repeat(String count_repeat)
    {
        this.count_repeat = count_repeat;
    }
}
